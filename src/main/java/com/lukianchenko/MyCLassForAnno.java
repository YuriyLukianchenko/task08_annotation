package com.lukianchenko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class MyCLassForAnno {
    @MyFirstannotation(name = "countOfLegs")
    int legs;
    String bukvar;
    @MyFirstannotation(name="tailCondition")
    boolean hasTail = true;
    int number1() {
        System.out.println("number1 was invoked");
        return 2;
    }
    String number2(int x) {
        System.out.println("number2 was invoked");
        return String.valueOf(x+1);

    }
    char number3(boolean b) {
        System.out.println("number3 was invoked");
        if(b==true) {
            return 't';
        } else {
            return 'f';
        }
    }
    void myMethod(String a, int ...args){
        Arrays.stream(args).forEach(x -> System.out.println(a + x));
    }
    void myMethod2(String ... args){
        Arrays.stream(args).forEach(x -> System.out.println(x));
    }

    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(MyCLassForAnno.class);
        MyCLassForAnno it = new MyCLassForAnno();
        Class classOfIt = it.getClass();
        Field[] fields = classOfIt.getDeclaredFields();
        for(Field field : fields ){
            logger.info("field:" + field.getName() + "  type:" + field.getType());
            if (field.getName() == "bukvar" ){
                try {
                    field.set(it,"abetka");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            Annotation[] annotations = field.getDeclaredAnnotations();
            for(Annotation annotation : annotations){
                logger.info(annotation);
            }
        }
        try {
            Method method1 = classOfIt.getDeclaredMethod("number1");
            method1.invoke(it);
            Method method2 = classOfIt.getDeclaredMethod("number2", int.class);
            method2.invoke(it,2);
            Method method3 = classOfIt.getDeclaredMethod("number3", boolean.class);
            method3.invoke(it,true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        logger.info("it.bukvar=" + it.bukvar);
        try {
            Method myMethod = classOfIt.getDeclaredMethod("myMethod",String.class, int[].class );
            myMethod.invoke(it,"bobi=",new int[]{1,2,3,4,5,78});
            Method myMethod2 = classOfIt.getDeclaredMethod("myMethod2", String[].class);
            myMethod2.invoke(it, (Object) new String[]{"we","ew"});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        //--------------------------------------
        logger.info("--------------");
        ObjectDescriptor about = new ObjectDescriptor(logger);
        logger.info(" object has next fields: ");
        Arrays.stream(about.fields).forEach(logger::info);
        logger.info("\n object has next constructors: ");
        Arrays.stream(about.constructors).forEach(logger::info);
        logger.info("\n object has next methods: ");
        Arrays.stream(about.methods).forEach(logger::info);



    }
}
