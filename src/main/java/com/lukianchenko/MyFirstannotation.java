package com.lukianchenko;

import java.lang.annotation.*;

@Documented
@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyFirstannotation {
    int number() default 134;
    String name();
}
