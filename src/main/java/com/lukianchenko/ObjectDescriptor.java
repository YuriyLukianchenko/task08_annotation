package com.lukianchenko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class ObjectDescriptor {
    Logger logger = LogManager.getLogger(ObjectDescriptor.class);
    public Field[] fields;
    public Constructor[] constructors;
    public Method[] methods;
    public ObjectDescriptor(Object obj){
        Class clazz = obj.getClass();
        fields = clazz.getDeclaredFields();
        constructors = clazz.getDeclaredConstructors();
        methods = clazz.getDeclaredMethods();
        logger.info("object class: " + clazz.toString());
    }


}


